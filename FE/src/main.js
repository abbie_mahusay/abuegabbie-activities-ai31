import 'bootstrap';
import 'bootstrap/dist/css/bootstrap.min.css';
import './assets/css/style.css'
import { library } from '@fortawesome/fontawesome-svg-core'
import { faChartBar } from '@fortawesome/free-solid-svg-icons'
import { faUser } from '@fortawesome/free-solid-svg-icons'
import { faLayerGroup } from '@fortawesome/free-solid-svg-icons'
import { faCog } from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'
import "chart.js"
import "hchs-vue-charts"
import Vue from 'vue'
import router from "./routes/router";
import App from "../src/App.vue";

library.add(faLayerGroup)
library.add(faChartBar)
library.add(faUser)
library.add(faCog)


Vue.component('font-awesome-icon', FontAwesomeIcon)

Vue.use(window.VueCharts);
Vue.config.productionTip = false;

new Vue({
  router,
  render: (h) => h(App),
}).$mount("#app");
